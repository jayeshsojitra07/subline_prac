import React, { Component } from 'react';
import { Modal } from 'antd';
import _ from 'lodash';
import Timer from "../Component/Timer";
import moment from "moment";
import 'antd/dist/antd.css';

class dashoard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible_popup: false,
            dish: {},
            allDish: []
        }
    }


    componentWillMount = () => {
        let allDishs = JSON.parse(sessionStorage.getItem("allDishs")) ? JSON.parse(sessionStorage.getItem("allDishs")) : []
        if (allDishs.length > 0) {
            this.setState({
                allDish: allDishs
            })
        }
    }

    // Save new dish
    addNewDish = (e) => {
        e.preventDefault();
        let allDishs = JSON.parse(sessionStorage.getItem("allDishs")) ? JSON.parse(sessionStorage.getItem("allDishs")) : []
        let { dish } = this.state;
        let id = allDishs.length + 1;
        dish.id = id;
        dish.is_dish = true;
        allDishs.push(dish)
        this.setState({
            visible_popup: false,
            allDish: allDishs,
            dish:{}
        })
        sessionStorage.setItem("allDishs", JSON.stringify(allDishs))


    }



    // set dish value in object
    dishonChange = (e) => {

        let { dish } = this.state;
        dish = Object.assign(dish, { [e.target.name]: e.target.value });
        this.setState({ dish })
    }


    renderDishList = () => {
        // let { allDish } = this.state;
        let allDishs = JSON.parse(sessionStorage.getItem("allDishs")) ? JSON.parse(sessionStorage.getItem("allDishs")) : []
        return _.map(allDishs, (item, i) => {
            if (item.is_dish) {
                return (
                    <tr key={i}>
                        <td>{item.id}</td>
                        <td>{item.dish_name}</td>
                        <td>{item.dish_price}</td>
                        <td> <Timer {...this.props} {...this.state} item={item} /></td>
                    </tr>
                )
            }
        })

    }


    // calcilate dish prepared time
    calculateTime = (item) => {


    }

    // Hide Popup
    hidePopup = () => {
        this.setState({
            visible_popup: false
        })
    }
    render() {
        let { dish } = this.state;
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12 mb-3 mt-3">
                        <div className="text-right">
                            <button className="btn btn-primary" onClick={(e) => { this.setState({ visible_popup: true }) }}>Add dish</button>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-body">
                                <table className="table table-border table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Remaining time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.renderDishList()}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal
                    title="Add dish"
                    visible={this.state.visible_popup}
                    onCancel={this.hidePopup}
                    footer={null}
                >
                    <form className="form" onSubmit={(e) => { this.addNewDish(e) }}>
                        <div className="form-group">
                            <label>Dish name</label>
                            <select className="form-control"
                                name="dish_name" onChange={(e) => { this.dishonChange(e) }}>
                                <option value=""></option>
                                <option value="a">A</option>
                                <option value="b">B</option>
                                <option value="c">C</option>
                                <option value="d">D</option>
                                <option value="e">E</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <label>Price</label>
                            <input type="number"
                                className="form-control"
                                required
                                onChange={(e) => { this.dishonChange(e) }}
                                name="dish_price" value={dish.dish_price || ''} />
                        </div>
                        <div className="form-group">
                            <label>Time</label>
                            <input type="number"
                                className="form-control"
                                required
                                onChange={(e) => { this.dishonChange(e) }}
                                name="dish_time" value={dish.dish_time || ''} />
                        </div>
                        <button type="submit" className="btn btn-primary" >Add</button>
                    </form>
                </Modal>
            </div>
        )
    }
}


export default dashoard