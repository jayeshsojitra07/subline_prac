import React, { Component } from 'react'
import moment from 'moment';
export default class Timer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mins: ''
        }
    }
    componentWillMount = () => {
        let { allDish } = this.props;
        let { mins } = this.state;

        // let allDishs = JSON.parse(sessionStorage.getItem("allDishs")) ? JSON.parse(sessionStorage.getItem("allDishs")) : []

        let index = allDish.findIndex(obj => obj.id === this.props.item.id)
        var duration = moment.duration({
            'minutes': this.props.item.dish_time
        });

        var min = '';
        var sec = '';
        var timestamp = new Date(0, 0, 0, 2, 10, 30);
        var interval = 1;
        setInterval(() => {
            timestamp = new Date(timestamp.getTime() + interval * 1000);

            duration = moment.duration(duration.asSeconds() - interval, 'seconds');
            min = duration.minutes();
            sec = duration.seconds();

            sec -= 1;
            
            if (min < 10 && min.length != 2) min = '0' + min;
            if (sec < 0 && min != 0) {
                min -= 1;
                sec = 59;
            }
            allDish[index]['dish_time'] = min;

            if (min == "00" || min == '0') {
                allDish[index]['is_dish'] = false;
            }
            this.setState({
                mins: min
            })
            sessionStorage.setItem("allDishs", JSON.stringify(allDish))
            return mins


        }, 1000);
    }



    render() {
        let { mins } = this.state;
        return (
            <div>
                {mins || '0'} Min
            </div>
        )
    }
}
